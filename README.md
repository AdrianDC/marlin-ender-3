# Marlin - Ender 3 series

<!-- Configurations: -->
<!-- markdownlint-disable MD033 -->

Acquire, configure and build the Marlin firmware for the Creality Ender 3 series 3D printer.

---

## Marlin features currently enabled

* **Ender 3 V2 :**

  * **Name :** Ender 3 V2 UltraSilent
  * **Motherboard :** Creality V4.2.2
  * **Axis steps :** Customized default values
  * **Homing positions :** Higher location
  * **Maximum position :** 200mm limit for safety
  * **User interface :** Scrolling filenames and prints counter
  * **Travel movements :** Default retract, curve and quick home
  * **Power features :** Disable powerloss recovery for MicroSD lifetime and enable autostart
  * **Hotend safeties :** Enable and configure idle hotend timeouts
  * **Babystep features :** Enable by default
  * **Bed leveling :** Enable BLTouch and related configurations

<!-- -->

---

## Ultimaker Cura - Preferences

### General

* **Interface**

  * **Theme :** Ultimaker Dark

<!-- -->

* **Viewport behavior**

  * **Center camera when item is selected :** **✓**
  * **Zoom toward mouse direction :** **✓**
  * **Automatically drop models to the build plate :** **✗**

<!-- -->

* **Opening and saving files**

  * **Scale large models :** **✓**
  * **Add machine prefix to job name :** **✗**

<!-- -->

* **Privacy**

  * **Send (anonymous) print information :** **✗**

### Settings

* **Quality**

  * **Layer Height :** **✓**
  * **Initial Layer Height :** **✓**
  * **Line Width :** **✓**
  * **Wall Line Width :** **✓**
  * **Support Line Width :** **✓**
  * **Initial Layer Width :** **✓**

<!-- -->

* **Walls**

  * **Wall Thickness :** **✓**
  * **Wall Line Count :** **✓**
  * **Z Seam Alignment :** **✓**
  * **Seam Corner Preference :** **✓**

<!-- -->

* **Top/Bottom**

  * **Top/Bottom Thickness :** **✓**
  * **Top Thickness :** **✓**
  * **Top Layers :** **✓**
  * **Bottom Thickness :** **✓**
  * **Bottom Layers :** **✓**
  * **Top/Bottom Pattern :** **✓**
  * **Bottom Pattern Initial Layer :** **✓**

<!-- -->

* **Infill**

  * **Infill Density :** **✓**
  * **Infill Line Distance :** **✓**
  * **Infill Pattern :** **✓**
  * **Infill Line Directions :** **✓**

<!-- -->

* **Material**

  * **Printing Temperature :** **✓**
  * **Printing Temperature Initial Layer :** **✓**
  * **Build Plate Temperature :** **✓**
  * **Build Plate Temperature Initial Layer :** **✓**

<!-- -->

* **Speed**

  * **Print Speed :** **✓**
  * **Infill Speed :** **✓**
  * **Wall Speed :** **✓**
  * **Top Surface Skin Speed :** **✓**
  * **Top/Bottom Speed :** **✓**
  * **Support Speed :** **✓**
  * **Support Infill Speed :** **✓**
  * **Support Interface Speed :** **✓**
  * **Travel Speed :** **✓**
  * **Initial Layer Speed :** **✓**
  * **Skirt/Brim Speed :** **✓**
  * **Enable Acceleration Control :** **✓**
  * **Print Acceleration :** **✓**
  * **Infill Acceleration :** **✓**
  * **Wall Acceleration :** **✓**
  * **Support Acceleration :** **✓**
  * **Travel Acceleration :** **✓**
  * **Enable Jerk Control :** **✓**
  * **Print Jerk :** **✓**
  * **Infill Jerk :** **✓**
  * **Wall Jerk :** **✓**
  * **Top/Bottom Jerk :** **✓**
  * **Travel Jerk :** **✓**
  * **Initial Layer Jerk :** **✓**
  * **Skirt/Brim Jerk :** **✓**

<!-- -->

* **Travel**

  * **Enable Retraction :** **✓**
  * **Retraction Distance :** **✓**
  * **Retraction Speed :** **✓**
  * **Combing Mode :** **✓**
  * **Z Hop When Retracted :** **✓**

<!-- -->

* **Cooling**

  * **Enable Print Cooling :** **✓**
  * **Fan Speed :** **✓**
  * **Initial Fan Speed :** **✓**
  * **Regular Fan Speed at Height :** **✓**
  * **Regular Fan Speed at Layer :** **✓**

<!-- -->

* **Support**

  * **Generate Support :** **✓**
  * **Support Extruder :** **✓**
  * **Support Structure :** **✓**
  * **Tree Support Branch Angle :** **✓**
  * **Tree Support Branch Distance :** **✓**
  * **Tree Support Branch Diamenter :** **✓**
  * **Tree Support Branch Diamenter Angle :** **✓**
  * **Tree Support Collision Resolution :** **✓**
  * **Support Placement :** **✓**
  * **Support Overhang Angle :** **✓**
  * **Support Pattern :** **✓**
  * **Support Wall Line Count :** **✓**
  * **Connect Support Lines :** **✓**
  * **Connect Support ZigZags :** **✓**
  * **Support Density :** **✓**
  * **Support Line Distance :** **✓**
  * **Support Infill Line Directions :** **✓**
  * **Enable Support Brim :** **✓**
  * **Support Brim Width :** **✓**
  * **Support Brim Line Count :** **✓**
  * **Support Z Distance :** **✓**
  * **Support X/Y Distance :** **✓**
  * **Minimum Support X/Y Distance :** **✓**

<!-- -->

* **Build Plate Adhesion**

  * **Enable Prime Blob :** **✓**
  * **Build Plate Adhesion Type :** **✓**
  * **Build Plate Adhesion Extruder :** **✓**
  * **Skirt Line Count:** **✓**
  * **Skirt Distance:** **✓**
  * **Skirt/Brim Minimum Length:** **✓**
  * **Brim Width :** **✓**
  * **Brim Line Count :** **✓**

<!-- -->

* **Dual Extrusion**

  * None

<!-- -->

* **Mesh Fixes**

  * **Union Overlapping Volumes :** **✓**
  * **Remove All Holes :** **✓**

<!-- -->

* **Special Modes**

  * **Spiralize Outer Contour :** **✓**
  * **Smooth Spiralized Contour :** **✓**

<!-- -->

* **Experimental**

  * **Enable Conical Support :** **✓**
  * **Conical Support Angle :** **✓**
  * **Connical Support Minimum Width :** **✓**
  * **Use Adaptive Layers :** **✓**

### Printers

#### Machine Settings

* **Printer - Ender 3 V2**

  * **Name :** Creality Ender 3 V2 (Template : Creality Ender-3 Pro)
  * **Z (Height) :** 200 mm
  * **Start G-code :**

    ```bash
    ; Ender 3 Custom Start G-code
    G92 E0 ; Reset Extruder
    G28 ; Home all axes

    G1 Z40.0 F3000 ; Move Z Axis up a little to prevent scratching of Heat Bed
    G1 X0.1 Y10 F5000.0 ; Move to start position

    M140 S{material_bed_temperature_layer_0} ; Start heating the bed, wait until target temperature reached
    M109 S200 T0 ; Start heating up the nozzle most of the way

    G92 E0 ; Reset Extruder
    G1 E-20 F2700 ; Retract Extruder a bit
    G92 E0 ; Reset Extruder

    M190 S{material_bed_temperature_layer_0} ; Start heating the bed, wait until target temperature reached
    M109 S{material_print_temperature_layer_0} ; Finish heating the nozzle

    G1 E19 F2700 ; Realign Extruder a bit
    G1 X0.1 Y20 Z0.3 F5000.0 ; Move to start position

    G92 E0 ; Reset Extruder
    G1 Z2.0 F3000 ; Move Z Axis up little to prevent scratching of Heat Bed
    G1 X0.1 Y20 Z0.3 F5000.0 ; Move to start position
    G1 X0.1 Y200.0 Z0.3 F1500.0 E15 ; Draw the first line
    G1 X0.7 Y200.0 Z0.3 F5000.0 ; Move to side a little
    G1 X0.7 Y20 Z0.3 F1500.0 E30 ; Draw the second line
    G92 E0 ; Reset Extruder
    G1 Z2.0 F3000 ; Move Z Axis up little to prevent scratching of Heat Bed
    G1 X5 Y20 Z0.3 F5000.0 ; Move over to prevent blob squish
    ```

---

## Dependencies

* [Marlin](https://github.com/MarlinFirmware/Marlin/tree/2.1.x): Firmware for 3D printers
* [Configurations](https://github.com/MarlinFirmware/Configurations): Configurations for 3D printers
* [gitlabci-local](https://pypi.org/project/gitlabci-local/): Launch .gitlab-ci.yml jobs locally
* [platformio](https://platformio.org): Open source, cross-platform IDE and Unified Debugger
