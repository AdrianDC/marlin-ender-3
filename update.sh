#/bin/sh
set -e

# Header
echo ''

# Variables
marlin_configurations_branch=''
marlin_configurations_url='https://github.com/MarlinFirmware/Configurations.git'
marlin_firmware_branch='2.1.x'
marlin_firmware_url='https://github.com/MarlinFirmware/Marlin.git'

# Clone Marlin sources
if [ ! -d ./Marlin ]; then
  git clone -b "${marlin_firmware_branch}" --depth 100 --no-tags --single-branch "${marlin_firmware_url}" ./Marlin
  echo ''
fi

# Update Marlin sources
cd ./Marlin/
git fetch origin "${marlin_firmware_branch}"
git reset --hard FETCH_HEAD
echo ''
cd - >/dev/null

# Apply Marlin patch: https://github.com/otioss/Marlin/commit/9e442b
cd ./Marlin/
sed -i 's#"G28O\\nG29"#"G28O\\nG29\\nM500"#' ./Marlin/src/lcd/e3v2/creality/dwin.cpp
git add -v ./Marlin/src/lcd/e3v2/creality/dwin.cpp
git -c user.email='otioss@protonmail.com' -c user.name='otioss' commit -m 'The mesh is not saved in EEPROM after the ABL finishes probing the bed' --no-edit
echo ''
cd - >/dev/null

# Cleanup Marlin sources
cd ./Marlin/
rm -rf ./.pio
find -name *.a -delete
cd - >/dev/null

# Detect Marlin configurations
if [ -z "${marlin_configurations_branch}" ]; then
  marlin_configurations_branch=$(grep '#define SHORT_BUILD_VERSION' ./Marlin/Marlin/Version.h | cut -d'"' -f2)
fi

# Clone Marlin configurations
if [ ! -d ./Configurations ]; then
  git clone -b "${marlin_configurations_branch}" --no-tags --single-branch "${marlin_configurations_url}" ./Configurations
  echo ''
fi

# Update Marlin configurations
cd ./Configurations/
git fetch origin "${marlin_configurations_branch}"
git reset --hard FETCH_HEAD
echo ''
cd - >/dev/null
