#/bin/sh
set -e

# Enable option function
enable_option() {
  printf " \033[1;33mINFO: \033[1;32mEnabling '${1}' in \033[1;37m${2##*/}: \033[0m"
  sed -i "s#//\(\#define ${1}\)#\1#g" "${2}"
  grep -m 1 --color=always "\([^\*] \{1,\}\|\)#define ${1}" "${2}"
}

# Disable option function
disable_option() {
  printf " \033[1;33mINFO: \033[1;32mDisabling '${1}' in \033[1;37m${2##*/}: \033[0m"
  sed -i "s#\(\|//\)\(\#define ${1}\)#//\2#g" "${2}"
  grep -m 1 --color=always "\([^\*] \{1,\}\|\)#define ${1}" "${2}"
}

# Set value function
set_value() {
  printf " \033[1;33mINFO: \033[1;32mSetting '${1}' = ${2} in \033[1;37m${3##*/}: \033[0m"
  sed -i "s#^\([ ]*\)\(\|//\)\(\#define ${1}\)\([ ]\{1,\}\)\([^/]\{1,\}[/]\{0,1\}\)\{1,\}\( // .*\|\)\$#\1\3\4${2}\6#g" "${3}"
  grep -m 1 --color=always "^\([ ]*\)#define ${1} " "${3}"
}

# Add value function
add_value() {
  printf " \033[1;33mINFO: \033[1;32mAdding '${1}'${2:+ = ${2}} in \033[1;37m${4##*/}: \033[0m"
  if ! grep -q "#define ${1}" "${4}"; then
    if [ ! -z "${2}" ]; then
      sed -i "/${3}/a #define ${1} ${2}" "${4}"
    else
      sed -i "/${3}/a #define ${1}" "${4}"
    fi
  else
    if [ ! -z "${2}" ]; then
      set_value "${1}" "${2}" "${4}"
    else
      enable_option "${1}" "${4}"
    fi
  fi
  grep -m 1 --color=always "\([^\*] \{1,\}\|\)#define ${1}${2:+ }" "${4}"
}

# Header
echo ''

# Create Marlin constants
configuration_folder='./Marlin/Marlin'
configuration_file="${configuration_folder}/Configuration.h"
configuration_advanced_file="${configuration_folder}/Configuration_adv.h"

# Import Marlin configurations
if [ "${CONFIGURATION_UI:-}" = 'MarlinUI' ]; then
  cp -rfv './Configurations/config/examples/Creality/Ender-3 V2/CrealityV422/MarlinUI/'* ./Marlin/Marlin/
else
  cp -rfv './Configurations/config/examples/Creality/Ender-3 V2/CrealityV422/CrealityUI/'* ./Marlin/Marlin/
fi
echo ''

# Configure Marlin platform
sed -i 's/^\(default_envs\) = .*/\1 = STM32F103RE_creality/g' ./Marlin/platformio.ini

# Configure firmware details
set_value 'CUSTOM_MACHINE_NAME' '"Ender 3 V2"' "${configuration_file}"
add_value 'WEBSITE_URL' '"adriandc.github.io"' 'CUSTOM_MACHINE_NAME' "${configuration_file}"

# Configure firmware motherboard
set_value 'MOTHERBOARD' 'BOARD_CREALITY_V422' "${configuration_file}"
#set_value 'MOTHERBOARD' 'BOARD_CREALITY_V427' "${configuration_file}"

# Configure MarlinUI orientation
if [ "${CONFIGURATION_UI:-}" = 'MarlinUI' ]; then
  enable_option 'DWIN_MARLINUI_PORTRAIT' "${configuration_file}"
  disable_option 'DWIN_MARLINUI_LANDSCAPE' "${configuration_file}"
fi

# Configure axis steps
set_value 'DEFAULT_AXIS_STEPS_PER_UNIT' '{ 80.00, 80.00, 400.00, 99.70 }' "${configuration_file}"

# Configure homing positions
set_value 'Z_AFTER_HOMING' '40' "${configuration_file}"

# Configure maximum positions
set_value 'Z_MAX_POS' '200' "${configuration_file}"

# Configure user interface
enable_option 'SCROLL_LONG_FILENAMES' "${configuration_advanced_file}"
enable_option 'PRINTCOUNTER' "${configuration_file}"
set_value 'PRINTCOUNTER_SAVE_INTERVAL' '15' "${configuration_file}"
set_value 'SERVICE_NAME_1' '"Cleanup"' "${configuration_advanced_file}"
set_value 'SERVICE_INTERVAL_1' '12' "${configuration_advanced_file}"

# Configure travel movements
set_value 'DEFAULT_RETRACT_ACCELERATION' '1000' "${configuration_file}"
enable_option 'QUICK_HOME' "${configuration_advanced_file}"

# Configure power features
disable_option 'POWER_LOSS_RECOVERY' "${configuration_advanced_file}"
enable_option 'MENU_ADDAUTOSTART' "${configuration_advanced_file}"

# Configure hotend safeties
enable_option 'HOTEND_IDLE_TIMEOUT' "${configuration_advanced_file}"
set_value 'HOTEND_IDLE_NOZZLE_TARGET' '150' "${configuration_advanced_file}"
set_value 'HOTEND_IDLE_BED_TARGET' '50' "${configuration_advanced_file}"

# Configure PID tuning
if [ "${CONFIGURATION_UI:-}" = 'MarlinUI' ]; then
  enable_option 'PID_AUTOTUNE_MENU' "${configuration_file}"
fi

# Configure preheat temperatures
# set_value 'PREHEAT_1_TEMP_HOTEND' '210' "${configuration_file}"
# set_value 'PREHEAT_1_TEMP_BED' '50' "${configuration_file}"
# set_value 'PREHEAT_2_LABEL' '"PETG"' "${configuration_file}"
# set_value 'PREHEAT_2_TEMP_HOTEND' '230' "${configuration_file}"
# set_value 'PREHEAT_2_TEMP_BED' '60' "${configuration_file}"
# set_value 'PREHEAT_2_FAN_SPEED' '128' "${configuration_file}"

# Configure babystep features
enable_option 'BABYSTEPPING' "${configuration_advanced_file}"
enable_option 'BABYSTEP_ALWAYS_AVAILABLE' "${configuration_advanced_file}"
set_value 'BABYSTEP_MULTIPLICATOR_Z' '1' "${configuration_advanced_file}"
enable_option 'DOUBLECLICK_FOR_Z_BABYSTEPPING' "${configuration_advanced_file}"
enable_option 'BABYSTEP_ZPROBE_OFFSET' "${configuration_advanced_file}"

# Configure bed leveling
disable_option 'Z_MIN_PROBE_USES_Z_MIN_ENDSTOP_PIN' "${configuration_file}"
enable_option 'USE_PROBE_FOR_Z_HOMING' "${configuration_file}"
disable_option 'PROBE_MANUALLY' "${configuration_file}"
enable_option 'BLTOUCH' "${configuration_file}"
set_value 'NOZZLE_TO_PROBE_OFFSET' '{ -43.5, -15.0, -2.05 }' "${configuration_file}"
set_value 'XY_PROBE_FEEDRATE' '(100*60)' "${configuration_file}"
set_value 'MULTIPLE_PROBING' '2' "${configuration_file}"
enable_option 'AUTO_BED_LEVELING_BILINEAR' "${configuration_file}"
enable_option 'RESTORE_LEVELING_AFTER_G28' "${configuration_file}"
set_value 'GRID_MAX_POINTS_X' '4' "${configuration_file}"
set_value 'GRID_MAX_POINTS_Y' '4' "${configuration_file}"
enable_option 'BED_TRAMMING_INCLUDE_CENTER' "${configuration_file}"
enable_option 'BED_TRAMMING_AUDIO_FEEDBACK' "${configuration_file}"
enable_option 'Z_SAFE_HOMING' "${configuration_file}"
set_value 'Z_SAFE_HOMING_X_POINT' '(X_BED_SIZE / 2)' "${configuration_file}"
set_value 'Z_SAFE_HOMING_Y_POINT' '(Y_BED_SIZE / 2)' "${configuration_file}"
set_value 'HOMING_FEEDRATE_MM_M' '{ (50*60), (50*60), (4*60) }' "${configuration_file}"
enable_option 'PROBE_OFFSET_WIZARD' "${configuration_advanced_file}"
disable_option 'BLTOUCH_DELAY' "${configuration_advanced_file}"
enable_option 'BLTOUCH_HS_MODE' "${configuration_advanced_file}"

# Enable advanced features
enable_option 'PREHEAT_SHORTCUT_MENU_ITEM' "${configuration_advanced_file}"
if [ "${CONFIGURATION_UI:-}" = 'MarlinUI' ]; then
  enable_option 'BROWSE_MEDIA_ON_INSERT' "${configuration_advanced_file}"
fi

# Footer
echo ''
